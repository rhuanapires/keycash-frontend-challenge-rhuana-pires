# :book:Menu

- [Sobre](#heart-sobre)
- [Instalação](#floppy_disk-instalação)
- [APK](#iphone-apk)
- [Tecnologias](#computer-tecnologias)
- [A Aplicação](#star-aplicação)
  - [Index](#index)
  - [Heroes](#heroes)
  - [Details](#details)
- [Autora](#raising_hand-autora)
  - [Contato](#iphone-contato)

# :heart: Sobre

O APP foi construindo visando a construção de um aplicativo que consome uma API de consulta de imóveis (http://5e148887bce1d10014baea80.mockapi.io/keycash/challenge) e exibe as opções disponíveis seguindo as regras de negócio pré estabelecidas. Além disso, é possível visualizar os detalhes e realizar buscas simples e avançadas, a partir de alguns parâmetros especificados!
A paginação também é dinâmica e determinada pelo usuário com um simples toque.
A aplicação mobile foi construída utilizando a biblioteca **React Native**.

# :floppy_disk: Instalação

**Baixe a aplicação e acesse a pasta do projeto na sua máquina local**<br>

**Instale as dependências e rode o projeto**<br>

_- Com Yarn_

```
1. yarn
2. yarn ios ou yarn android
```

_- Com NPM_

```
1. npm install
2. npm run ios ou npm run android
```
# :iphone: APK

Para gerar o APK, execute os seguintes comandos no terminal dentro da pasta da aplicação

_- Com YARN_
```
1. yarn android:bundle
2. yarn android:generate:release
```

_- Com NPM_
```
1. npm run android:bundle
2. npm run android:generate:release
```

Ao finalizar a execução dos comandos, o APK estará disponível na pasta /android/app/build/outputs/apk/release/

# :computer: Tecnologias

- [React-Native](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org)
- [ESLint](https://eslint.org)
- [React Navigation](https://reactnavigation.org)
- [Styled Components](https://styled-components.com)
- [Axios](https://github.com/axios/axios)
- [React-Native Vector Icons](https://oblador.github.io/react-native-vector-icons/)

# :star: Aplicação

### Index

<div align="center">
<img src="https://i.ibb.co/VLG9Nqg/Simulator-Screen-Shot-i-Phone-13-2021-11-05-at-16-05-27.png" alt="Index Screen" width="250"/>   
</div>

### Deep Search

<div align="center">
<img src="https://i.ibb.co/hszqWdR/Simulator-Screen-Shot-i-Phone-13-2021-11-05-at-16-06-39.png" alt="Deep Search" width="250"/>   
</div>

### Details

<div align="center">
<img src="https://i.ibb.co/9cGQbMW/Simulator-Screen-Shot-i-Phone-13-2021-11-05-at-16-06-47.png" alt="Details Screen" width="250"/>   
</div>
  
# :raising_hand: Autora

<div align="center">
  
  ![Rhuana Pires](https://firebasestorage.googleapis.com/v0/b/testefortbrasil.appspot.com/o/profile%2Fprofile2.png?alt=media&token=70db12b3-c14b-4f50-8845-8033214a0692)
  
 <br />
 <sub><b>Rhuana Pires</b><br>
  Graduanda em Sistemas de Informação - Unichristus<br />
  :purple_heart: Mobile Development<br />
  :purple_heart: UI/UX Design</sub>
  <br/><br/>
  
  Feito com :purple_heart: por **Rhuana Pires**
  
</div>

### :iphone: Contato

<div align="center">

[![Twitter Badge](https://img.shields.io/badge/-@rhuanapires-1ca0f1?style=flat-square&labelColor=1ca0f1&logo=twitter&logoColor=white&link=https://twitter.com/rhuanapires)](https://twitter.com/rhuanapires) [![Linkedin Badge](https://img.shields.io/badge/-Rhuana%20Pires-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/rhuana-pires/)](https://www.linkedin.com/in/rhuana-pires/)
[![Gmail Badge](https://img.shields.io/badge/-rhuanapires@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:rhuanapires@gmail.com)](mailto:rhuanapires@gmail.com)

</div>