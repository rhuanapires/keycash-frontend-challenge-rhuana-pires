import {Dimensions} from 'react-native';
import {moderateScale} from '~/modules';
const {width} = Dimensions.get('window');
const {height} = Dimensions.get('window');

export default {
  topSpacing: moderateScale(20),
  bottomSpacing: moderateScale(10),
  sceneSpacing: moderateScale(22),
  sectionSpacing: moderateScale(40),
  minimumSpacing: moderateScale(4),
  smallerSpacing: moderateScale(8),
  smallSpacing: moderateScale(12),
  normalSpacing: moderateScale(16),
  mediumSpacing: moderateScale(20),
  largeSpacing: moderateScale(32),
  giantSpacing: moderateScale(72),
  widthScreen: (percent = 100) => (percent / 100) * width,
  heightScreen: (percent = 100) => (percent / 100) * height,
} as Spacings;
