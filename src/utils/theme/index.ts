import {default as colors} from './colors';
import {default as radius} from './radius';
import {default as spacings} from './spacings';

export {default as getShadow} from './get-shadow';
export {default as getFontSize} from './typography';

const theme = {
  colors,
  spacings,
  radius,
};

export default theme;
