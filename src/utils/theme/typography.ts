type variants =
  | 'largeTitle'
  | 'title1'
  | 'title2'
  | 'title3'
  | 'title4'
  | 'headline'
  | 'body'
  | 'callout'
  | 'subhead'
  | 'footnote'
  | 'caption1'
  | 'caption2';

const FONTS_VARIANTS = {
  largeTitle: `
    fontSize: 32px;
    lineHeight: 40px;
  `,
  title1: `
    fontSize: 26px;
    lineHeight: 31px;
  `,
  title2: `
    fontSize: 20px;
    lineHeight: 22px;
  `,
  title3: `
    fontSize: 19px;
    lineHeight: 22px;
  `,
  title4: `
    fontSize: 18px;
    lineHeight: 19px;
  `,
  headline: `
    fontSize: 17px;
    lineHeight: 21px;
  `,
  callout: `
  fontSize: 15px;
  lineHeight: 21px;
  `,
  body: `
    fontSize: 14px;
    lineHeight: 17px;
  `,
  subhead: `
    fontSize: 12px;
    lineHeight: 19px;
  `,
  footnote: `
    fontSize: 11px;
    lineHeight: 14px;
  `,
  caption1: `
    fontSize: 10px;
    lineHeight: 13px;
  `,
  caption2: `
    fontSize: 10px;
    lineHeight: 10px;
  `,
} as TypographyTheme;

export default function getFontSize(variant: variants): string {
  return FONTS_VARIANTS[variant];
}
