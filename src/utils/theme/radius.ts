import {moderateScale} from '~/modules';

export default {
  smallRadius: moderateScale(4),
  mediumRadius: moderateScale(8),
  largeRadius: moderateScale(14),
} as Radius;
