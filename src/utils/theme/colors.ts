export default {
  primary: '#fE1187',
  secondary: '#8D074E',
  terciary: '#fEB233',
  ternary: '#fEAA2b',
  warning: '#FFB02A',
  error: '#9D1119',
  info: '#41B1FA',
  success: '#D3FE33',
  text: {
    lowPure: '#000000',
    lowDark: '#292929',
    lowMedium: '#666666',
    lowLight: '#A3A3A3',
    highPure: '#FFFFFF',
    highDark: '#F5F5F5',
    highMedium: '#E0E0E0',
    highLight: '#CCCCCC',
  },
} as Colors;
