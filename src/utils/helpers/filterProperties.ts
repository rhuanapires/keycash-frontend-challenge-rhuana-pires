export function filterProperties(property: Property[]): Property[] {
  const filteredList = property.filter(
    item =>
      item.publish === true &&
      item.address?.geolocation?.lat &&
      item.address?.geolocation?.lng,
  );

  filteredList.sort((a, b) => {
    if (a.price < b.price) {
      return 1;
    }
    if (a.price > b.price) {
      return -1;
    }
    return 0;
  });

  return filteredList;
}

export function searchByName(input: string, property: Property[]): Property[] {
  const filter = normalizeInput(input);

  const filteredList = property.filter(property => {
    return normalizeInput(property.address.formattedAddress).includes(filter);
  });

  return filteredList;
}

export function normalizeInput(value: string): string {
  const normalizedValue = value
    .trim()
    .toLowerCase()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '');

  return normalizedValue;
}

export function completeSearch(
  address: string,
  minSize: string,
  maxSize: string,
  minValue: string,
  maxValue: string,
  selectedRooms: number,
  selectedBathrooms: number,
  selectedGarage: number,
  property: Property[],
) {
  const filteredAddress = normalizeInput(address);

  const filteredList = property.filter(property => {
    return (
      normalizeInput(property.address.formattedAddress).includes(
        filteredAddress,
      ) &&
      property.bathrooms >= selectedBathrooms + 1 &&
      property.bedrooms >= selectedRooms + 1 &&
      property.parkingSpaces >= selectedGarage + 1 &&
      (maxValue ? property.price <= parseInt(maxValue) : true) &&
      (minValue ? property.price >= parseInt(minValue) : true) &&
      (maxSize ? property.usableArea <= parseInt(maxSize) : true) &&
      (minSize ? property.usableArea >= parseInt(minSize) : true)
    );
  });

  return filteredList;
}
