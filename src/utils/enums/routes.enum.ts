export enum Routes {
  HOME = 'HOME',
  PROPERTY_DETAILS = 'PROPERTY_DETAILS',
  SEARCH_TOOL = 'SEARCH_TOOL',
}
