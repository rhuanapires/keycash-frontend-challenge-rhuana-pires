declare type Colors = {
  primary: string;
  secondary: string;
  terciary: string;
  ternary: string;
  warning: string;
  error: string;
  info: string;
  success: string;
  text: TextColors;
};

declare type TextColors = {
  lowPure: string;
  lowDark: string;
  lowMedium: string;
  lowLight: string;
  highPure: string;
  highDark: string;
  highMedium: string;
  highLight: string;
};

declare type Spacings = {
  topSpacing: number;
  bottomSpacing: number;
  sceneSpacing: number;
  sectionSpacing: number;
  minimumSpacing: number;
  smallSpacing: number;
  mediumSpacing: number;
  largeSpacing: number;
  giantSpacing: number;
  widthScreen: (percent?: number) => number;
  heightScreen: (percent?: number) => number;
};

declare type TypographyTheme = {
  largeTitle: string;
  title1: string;
  title2: string;
  title3: string;
  title4: string;
  headline: string;
  body: string;
  callout: string;
  subhead: string;
  footnote: string;
  caption1: string;
  caption2: string;
};

declare type Radius = {
  smallRadius: number;
  mediumRadius: number;
  largeRadius: number;
};
