import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    colors: Colors;
    spacings: Spacings;
    radius: Radius;
  }
}
