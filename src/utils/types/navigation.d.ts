import {RouteProp} from '@react-navigation/core';
import {Routes} from '../enums';

export type NavigationRouteParams = {
  [Routes.HOME]: {newList: Property};
  [Routes.PROPERTY_DETAILS]: {property: Property};
  [Routes.SEARCH_TOOL]: {properties: Property[]};
};

export type NavigationParams = {
  [key: string]: any;
};

export type InfoRoute = RouteProp<
  NavigationRouteParams,
  Routes.PROPERTY_DETAILS
>;

export type SearchToolRoute = RouteProp<
  NavigationRouteParams,
  Routes.SEARCH_TOOL
>;
