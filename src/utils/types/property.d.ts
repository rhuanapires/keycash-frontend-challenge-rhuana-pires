declare type Property = {
  id: string;
  address: Address;
  images: string[];
  price: number;
  bathrooms: number;
  bedrooms: number;
  parkingSpaces: number;
  usableArea: number;
  publish: boolean;
};

declare type Address = {
  formattedAddress: string;
  geolocation?: Geolocation;
};

declare type Geolocation = {
  lat: number;
  lng: number;
};
