import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://5e148887bce1d10014baea80.mockapi.io/keycash',
});

export default instance;
