export {default as Home} from './home';
export {default as PropertyDetails} from './property-details';
export {default as SearchTool} from './search-tool';
