import React, {Dispatch, FC, SetStateAction} from 'react';
import {ScrollView} from 'react-native';
import {
  Button,
  MainHeader,
  MultipleInputs,
  SceneWrapper,
  Selector,
} from '~/components';

type Props = {
  setMinValue: Dispatch<SetStateAction<string>>;
  setMaxValue: Dispatch<SetStateAction<string>>;
  maxMinSize: Dispatch<SetStateAction<string>>;
  setMaxSize: Dispatch<SetStateAction<string>>;
  address: Dispatch<SetStateAction<string>>;
  selectedRooms: number;
  setSelectedRooms: Dispatch<SetStateAction<number>>;
  selectedBathrooms: number;
  setSelectedBathrooms: Dispatch<SetStateAction<number>>;
  selectedGarage: number;
  setSelectedGarage: Dispatch<SetStateAction<number>>;
  doResearch: () => void;
};
const SearchTool: FC<Props> = ({
  setMinValue,
  setMaxValue,
  maxMinSize,
  setMaxSize,
  address,
  doResearch,
  setSelectedRooms,
  selectedRooms,
  selectedBathrooms,
  setSelectedBathrooms,
  setSelectedGarage,
  selectedGarage,
}) => {
  return (
    <SceneWrapper>
      <ScrollView>
        <MainHeader onChangeInput={address} />
        <MultipleInputs
          label="Valor"
          onChangeLeftInput={setMinValue}
          onChangeRightInput={setMaxValue}
          maxLength={7}
          leftPlaceholder="Valor Mínimo"
          rightPlaceholder="Valor Máximo"
        />
        <MultipleInputs
          label="Tamanho"
          onChangeLeftInput={maxMinSize}
          onChangeRightInput={setMaxSize}
          maxLength={3}
          leftPlaceholder="Tamanho mínimo"
          rightPlaceholder="Tamanho máximo"
        />
        <Selector
          label="Quartos"
          selected={selectedRooms}
          onSelect={setSelectedRooms}
        />
        <Selector
          label="Banheiros"
          selected={selectedBathrooms}
          onSelect={setSelectedBathrooms}
        />
        <Selector
          label="Vagas de Garagem"
          selected={selectedGarage}
          onSelect={setSelectedGarage}
        />
        <Button title="Buscar" icon="chevron-right" onPress={doResearch} />
      </ScrollView>
    </SceneWrapper>
  );
};

export default SearchTool;
