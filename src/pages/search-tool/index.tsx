import React, {FC, useState} from 'react';
import {useRoute} from '@react-navigation/core';
import {navigate} from '~/navigation/actions';
import {completeSearch} from '~/utils';
import {Routes} from '~/utils/enums';
import {SearchToolRoute} from '~/utils/types';
import SearchTool from './SearchTool';

const SearchToolContainer: FC = () => {
  const {params} = useRoute<SearchToolRoute>();
  const [minValue, setMinValue] = useState('');
  const [maxValue, setMaxValue] = useState('');
  const [minSize, setMinSize] = useState('');
  const [maxSize, setMaxSize] = useState('');
  const [address, setAddress] = useState('');
  const [selectedRooms, setSelectedRooms] = useState(-1);
  const [selectedBathrooms, setSelectedBathrooms] = useState(-1);
  const [selectedGarage, setSelectedGarage] = useState(-1);

  const doResearch = () => {
    const newList = completeSearch(
      address,
      minSize,
      maxSize,
      minValue,
      maxValue,
      selectedRooms,
      selectedBathrooms,
      selectedGarage,
      params?.properties,
    );
    navigate(Routes.HOME, {newList});
  };

  return (
    <SearchTool
      setMinValue={setMinValue}
      setMaxValue={setMaxValue}
      maxMinSize={setMinSize}
      setMaxSize={setMaxSize}
      address={setAddress}
      doResearch={doResearch}
      selectedRooms={selectedRooms}
      setSelectedRooms={setSelectedRooms}
      selectedBathrooms={selectedBathrooms}
      setSelectedBathrooms={setSelectedBathrooms}
      selectedGarage={selectedGarage}
      setSelectedGarage={setSelectedGarage}
    />
  );
};

export default SearchToolContainer;
