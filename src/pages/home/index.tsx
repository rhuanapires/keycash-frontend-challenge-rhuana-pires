import React, {FC, useEffect, useMemo, useState} from 'react';
import {useRoute} from '@react-navigation/core';
import instance from '~/services';
import {filterProperties, searchByName} from '~/utils';
import {InfoRoute} from '~/utils/types';
import Home from './Home';

const HomeContainer: FC = () => {
  const [properties, setProperties] = useState<Property[]>([]);
  const [loading, setLoading] = useState(true);
  const [searchInput, setSearchInput] = useState('');
  const [filteredProperties, setFilteredProperties] = useState<Property[]>([]);
  const [itemsByPage, setItemsByPage] = useState(5);
  const [totalPages, setTotalPages] = useState<number>(0);
  const [actualPage, setActualPage] = useState(1);

  const getProperties = async () => {
    try {
      const {data} = await instance.get<Property[]>('/challenge');
      const filteredList = filterProperties(data);
      setProperties(filteredList);
      setFilteredProperties(filteredList);
    } catch (error) {
      console.log('erro', error);
    } finally {
      setLoading(false);
    }
  };

  const {params} = useRoute<InfoRoute>();

  useEffect(() => {
    params ? setFilteredProperties(params?.newList) : getProperties();
  }, [params]);

  useEffect(() => {
    getPages();
  }, [filteredProperties, itemsByPage]);

  const changeSearchInput = (value: string) => {
    setSearchInput(value);
  };

  const doSearch = () => {
    setLoading(true);
    const newList = searchByName(searchInput, properties);
    setFilteredProperties(newList);
    setActualPage(1);
    setLoading(false);
  };

  const getPages = () => {
    const numberPages = filteredProperties.length / itemsByPage;
    setTotalPages(
      Number.isInteger(numberPages)
        ? Math.trunc(numberPages)
        : Math.trunc(numberPages + 1),
    );
  };

  const nextPage = () => {
    setActualPage(actualPage + 1);
  };

  const backPage = () => {
    setActualPage(actualPage - 1);
  };

  const onChangeQuantityByPage = (quantity: number) => {
    setItemsByPage(quantity);
    setActualPage(1);
  };

  const valuesToShow = useMemo(() => {
    const end = itemsByPage * actualPage;
    const start = end - itemsByPage;
    return filteredProperties.slice(start, end);
  }, [itemsByPage, actualPage, filteredProperties]);

  return (
    <Home
      properties={valuesToShow}
      loading={loading}
      onChangeInput={changeSearchInput}
      doSearch={doSearch}
      actualPage={actualPage}
      totalPages={totalPages}
      changeItemsByPage={onChangeQuantityByPage}
      nextPage={nextPage}
      backPage={backPage}
      listProperties={properties}
    />
  );
};

export default HomeContainer;
