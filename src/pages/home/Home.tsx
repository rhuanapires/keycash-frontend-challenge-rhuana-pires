import React, {FC} from 'react';
import {ListRenderItem} from 'react-native';
import {
  Card,
  Dropdown,
  If,
  LoadingPage,
  MainHeader,
  Pagination,
  SceneWrapper,
} from '~/components';
import {Footer, List} from './styles';

type Props = {
  properties: Property[];
  listProperties: Property[];
  loading: boolean;
  onChangeInput: (value: string) => void;
  doSearch: () => void;
  nextPage: () => void;
  backPage: () => void;
  changeItemsByPage: (quantity: number) => void;
  actualPage: number;
  totalPages: number;
};

const renderItem: ListRenderItem<Property> = ({item}) => (
  <Card key={item.id} property={item} />
);

const Home: FC<Props> = ({
  properties,
  loading,
  onChangeInput,
  doSearch,
  actualPage,
  totalPages,
  nextPage,
  backPage,
  changeItemsByPage,
  listProperties,
}) => {
  return (
    <SceneWrapper>
      <If condition={loading}>
        <LoadingPage />
      </If>
      <If condition={!loading}>
        <>
          <List
            data={properties}
            renderItem={renderItem}
            keyExtractor={item => item.id}
            initialNumToRender={5}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={
              <MainHeader
                onChangeInput={onChangeInput}
                doSearch={doSearch}
                enableButton
                properties={listProperties}
              />
            }
            ListFooterComponent={
              <If condition={!!properties.length}>
                <Footer>
                  <Dropdown changeItemsByPage={changeItemsByPage} />
                  <Pagination
                    actualPage={actualPage}
                    nextPage={nextPage}
                    backPage={backPage}
                    totalPages={totalPages}
                  />
                </Footer>
              </If>
            }
          />
        </>
      </If>
    </SceneWrapper>
  );
};

export default Home;
