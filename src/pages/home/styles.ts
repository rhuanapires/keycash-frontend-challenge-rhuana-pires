import {FlatList} from 'react-native';
import {styled} from '~/modules';

export const Separator = styled.View`
  height: 20px;
`;

export const List = styled(FlatList as new () => FlatList<Property>).attrs({
  ItemSeparatorComponent: Separator,
})``;

export const Footer = styled.View`
  padding: 20px 0px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
