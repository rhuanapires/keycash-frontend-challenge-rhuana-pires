import {styled} from '~/modules';
import {getFontSize} from '~/utils';

export const Address = styled.Text`
  ${getFontSize('title2')}
  color: ${props => props.theme.colors.text.lowMedium};
  font-weight: 700;
  text-align: left;
`;

export const Value = styled(Address)`
  color: ${props => props.theme.colors.primary};
`;
export const TextWrapper = styled.View`
  padding: 10px 20px;
`;

export const ContentWrapper = styled.ScrollView``;

export const ChildrenWrapper = styled.SafeAreaView``;

export const SubCategories = styled.View`
  padding: 10px;
`;
