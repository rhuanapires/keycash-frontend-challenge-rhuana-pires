import React, {FC} from 'react';
import {ImagesCarousel, TextCategory} from '~/components';
import {
  Address,
  ChildrenWrapper,
  ContentWrapper,
  SubCategories,
  TextWrapper,
  Value,
} from './styles';

type Props = {
  property: Property;
};

const PropertyDetails: FC<Props> = ({property}) => {
  return (
    <ChildrenWrapper>
      <ContentWrapper>
        <ImagesCarousel images={property.images} />
        <TextWrapper>
          <Address> {property.address.formattedAddress}</Address>
          <Value> {`R$ ${property.price},00`}</Value>
          <SubCategories>
            <TextCategory
              name="Área"
              title={`${property.usableArea}m²`}
              icon="ruler"
            />
            <TextCategory name="Quartos" title={property.bedrooms} icon="bed" />
            <TextCategory
              name="Banheiros"
              title={property.bathrooms}
              icon="shower"
            />
            <TextCategory
              name="Vagas"
              title={property.parkingSpaces}
              icon="car"
            />
          </SubCategories>
        </TextWrapper>
      </ContentWrapper>
    </ChildrenWrapper>
  );
};

export default PropertyDetails;
