import React, {FC} from 'react';
import {useRoute} from '@react-navigation/core';
import {InfoRoute} from '~/utils/types';
import PropertyDetails from './PropertyDetails';

type Props = {
  property: Property;
};

const PropertyDetailsContainer: FC<Props> = () => {
  const {params} = useRoute<InfoRoute>();

  return <PropertyDetails property={params.property} />;
};

export default PropertyDetailsContainer;
