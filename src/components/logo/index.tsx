import React, {FC} from 'react';
import {ImageLogo} from './styles';

const Logo: FC = () => {
  return <ImageLogo source={{uri: 'https://i.ibb.co/3c4J502/7584.png'}} />;
};

export default Logo;
