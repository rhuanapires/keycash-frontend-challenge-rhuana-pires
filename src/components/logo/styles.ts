import {FastImage, styled} from '~/modules';

export const ImageLogo = styled(FastImage).attrs({
  resizeMode: 'contain',
})`
  width: 100%;
  min-height: 100px;
  justify-content: center;
`;
