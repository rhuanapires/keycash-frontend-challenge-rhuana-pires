import React, {FC} from 'react';
import {TouchableOpacityProps} from 'react-native';
import If from '../if';
import Loading from '../loading';
import {ContainerButton, Content, Icon} from './styles';

type Props = TouchableOpacityProps & {
  icon: string;
  loading?: boolean;
  disabled?: boolean;
};

const SmallButton: FC<Props> = ({icon, loading, disabled, ...props}) => {
  return (
    <ContainerButton disabled={disabled || loading} {...props}>
      <Content>
        <If condition={loading || false}>
          <Loading />
        </If>
        <If condition={!loading}>
          <Icon name={icon} />
        </If>
      </Content>
    </ContainerButton>
  );
};

export default SmallButton;
