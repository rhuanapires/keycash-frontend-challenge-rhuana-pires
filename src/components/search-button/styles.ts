import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import styled from 'styled-components/native';
import {moderateScale} from '~/modules';
import {getShadow} from '~/utils';

export const ContainerButton = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.primary};
  border-radius: ${props => props.theme.radius.mediumRadius}px;
  justify-content: center;
  height: 40px;
`;

export const Content = styled.View`
  align-items: center;
  justify-content: center;
`;

export const Icon = styled(MaterialIcon)`
  color: ${props => props.theme.colors.text.highPure};
  font-size: ${moderateScale(17)}px;
  padding: 0px 15px;
`;
