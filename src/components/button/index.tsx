import React, {FC} from 'react';
import {TouchableOpacityProps} from 'react-native';
import If from '../if';
import Loading from '../loading';
import {ContainerButton, Content, Icon, Text} from './styles';

type Props = TouchableOpacityProps & {
  title: string;
  icon: string;
  loading?: boolean;
  disabled?: boolean;
};

const Button: FC<Props> = ({title, icon, loading, disabled, ...props}) => {
  return (
    <ContainerButton disabled={disabled || loading} {...props}>
      <Content>
        <If condition={loading || false}>
          <Loading />
        </If>
        <If condition={!loading}>
          <Text>{title}</Text>
          <Icon name={icon} />
        </If>
      </Content>
    </ContainerButton>
  );
};

export default Button;
