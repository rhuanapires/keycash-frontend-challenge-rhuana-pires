import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import styled from 'styled-components/native';
import {moderateScale} from '~/modules';
import {getFontSize} from '~/utils';

export const ContainerButton = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.primary};
  justify-content: center;
  border-radius: ${props => props.theme.radius.mediumRadius}px;
  align-items: center;
  height: 40px;
`;

export const Content = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

export const Text = styled.Text`
  font-weight: bold;
  ${getFontSize('callout')}
  color: ${props => props.theme.colors.text.highPure};
  margin-right: 10px;
`;

export const Icon = styled(MaterialIcon)`
  color: ${props => props.theme.colors.text.highPure};
  margin-left: 10px;
  font-size: ${moderateScale(17)}px;
`;
