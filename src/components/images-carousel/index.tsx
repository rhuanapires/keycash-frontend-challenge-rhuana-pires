import React, {FC, useState} from 'react';
import {Pagination} from 'react-native-snap-carousel';
import {CarouselItem, ImageSlider} from './styles';

type Props = {
  images: string[];
};

const ImagesCarousel: FC<Props> = ({images}) => {
  const [carouselItem, setCarouselItem] = useState(1);

  const renderItem = ({item}) => (
    <CarouselItem source={{uri: item}} resizeMode="cover" />
  );

  return (
    <>
      <ImageSlider
        data={images}
        renderItem={renderItem}
        onSnapToItem={index => setCarouselItem(index)}
      />
      <Pagination
        dotsLength={images.length}
        activeDotIndex={carouselItem}
        containerStyle={{
          paddingVertical: 10,
        }}
        dotStyle={{
          width: 50,
          height: 10,
          borderRadius: 5,
          backgroundColor: '#fE1187',
        }}
        dotContainerStyle={{marginHorizontal: 5}}
        inactiveDotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          backgroundColor: '#666666',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    </>
  );
};

export default ImagesCarousel;
