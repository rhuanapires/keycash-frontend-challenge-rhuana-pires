import {Dimensions} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import styled from 'styled-components/native';

const {width} = Dimensions.get('screen');

const CAROUSEL_WIDTH = width;
const CAROUSEL_HEIGHT = CAROUSEL_WIDTH * 0.75;

export const CarouselItem = styled.ImageBackground`
  width: ${CAROUSEL_WIDTH}px;
  height: ${CAROUSEL_HEIGHT}px;
  border-radius: 50px;
`;

export const Container = styled.View`
  position: relative;
  background-color: ${props => props.theme.colors.text.highDark};
  flex: 1;
`;

export const ImageSlider = styled(Carousel).attrs({
  sliderWidth: CAROUSEL_WIDTH,
  itemWidth: CAROUSEL_WIDTH,
  sliderHeight: CAROUSEL_WIDTH,
  autoplay: true,
  autoplayInterval: 3000,
  loop: true,
})`
  border-radius: 30px;
`;

export const WrapperCarouselItem = styled.TouchableOpacity``;
