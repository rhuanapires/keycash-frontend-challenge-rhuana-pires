import React, {FC} from 'react';
import DismissKeyboard from '../dismiss-keyboard';
import {ChildrenWrapper, ViewWrapper} from './styles';

const SceneWrapper: FC = ({children}) => {
  return (
    <DismissKeyboard>
      <ChildrenWrapper>
        <ViewWrapper>{children}</ViewWrapper>
      </ChildrenWrapper>
    </DismissKeyboard>
  );
};

export default SceneWrapper;
