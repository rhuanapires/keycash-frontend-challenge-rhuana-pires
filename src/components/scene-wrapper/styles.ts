import {styled} from '~/modules';

export const ChildrenWrapper = styled.SafeAreaView`
  background-color: ${props => props.theme.colors.text.highDark};
  flex: 1;
`;

export const ViewWrapper = styled.View`
  margin: 0px 20px;
`;
