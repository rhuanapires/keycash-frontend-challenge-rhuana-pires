import React, {FC} from 'react';
import {Container, SpinnnerLoading} from './styles';

const Loading: FC = () => (
  <Container>
    <SpinnnerLoading />
  </Container>
);

export default Loading;
