import Spinner from 'react-native-spinkit';
import styled from 'styled-components/native';
import {moderateScale} from '~/modules';

export const Container = styled.View`
  padding: 10px;
  flex-direction: row;
  justify-content: center;
`;

export const SpinnnerLoading = styled(Spinner).attrs(props => ({
  size: moderateScale(20),
  type: 'Circle',
  color: props.theme.colors.terciary,
}))``;
