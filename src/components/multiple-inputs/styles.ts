import {TextInput} from 'react-native';
import {moderateScale, styled} from '~/modules';
import {getFontSize, getShadow} from '~/utils';

export const InputWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 0px;
`;

export const Input = styled(TextInput)`
  height: ${moderateScale(35)}px;
  ${getFontSize('callout')};
  margin: 0px 15px;
  flex: 1;
`;

export const ContainerTextInput = styled.View`
  flex-direction: row;
  background-color: ${props => props.theme.colors.text.highPure};
  border-radius: ${props => props.theme.radius.largeRadius}px;
  justify-content: center;
  align-items: center;
  margin-right: 10px;
  padding: 5px;
  flex: 1;
  ${getShadow()};
`;

export const Title = styled.Text`
  ${getFontSize('callout')};
  color: ${props => props.theme.colors.text.lowMedium};
  font-weight: bold;
`;
