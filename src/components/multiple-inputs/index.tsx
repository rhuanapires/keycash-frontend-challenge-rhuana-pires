import React, {FC} from 'react';
import {ContainerTextInput, Input, InputWrapper, Title} from './styles';

type Props = {
  label: string;
  maxLength: number;
  onChangeLeftInput: (value: string) => void;
  onChangeRightInput: (value: string) => void;
  leftPlaceholder: string;
  rightPlaceholder: string;
};

const MultipleInputs: FC<Props> = ({
  label,
  onChangeLeftInput,
  onChangeRightInput,
  maxLength,
  leftPlaceholder,
  rightPlaceholder,
}) => {
  return (
    <>
      <Title>{label}</Title>
      <InputWrapper>
        <ContainerTextInput>
          <Input
            maxLength={maxLength}
            onChangeText={onChangeLeftInput}
            keyboardType="numeric"
            placeholder={leftPlaceholder}
          />
        </ContainerTextInput>
        <ContainerTextInput>
          <Input
            maxLength={maxLength}
            onChangeText={onChangeRightInput}
            keyboardType="numeric"
            placeholder={rightPlaceholder}
          />
        </ContainerTextInput>
      </InputWrapper>
    </>
  );
};

export default MultipleInputs;
