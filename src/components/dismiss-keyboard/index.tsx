import React, {FC} from 'react';
import {Keyboard, TouchableWithoutFeedback} from 'react-native';

interface Props {
  children: JSX.Element | JSX.Element[];
}

const DismissKeyboard: FC<Props> = ({children}) => (
  <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
    {children}
  </TouchableWithoutFeedback>
);

export default DismissKeyboard;
