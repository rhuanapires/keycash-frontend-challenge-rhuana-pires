import React, {FC} from 'react';
import SmallButton from '../small-button';
import {PaginationWrapper, Value} from './styles';

type Props = {
  actualPage: number;
  nextPage: () => void;
  backPage: () => void;
  totalPages: number;
};

const Pagination: FC<Props> = ({
  actualPage,
  nextPage,
  backPage,
  totalPages,
}) => {
  return (
    <PaginationWrapper>
      <SmallButton
        icon="chevron-left"
        onPress={backPage}
        disabled={actualPage === 1}
      />
      <Value>{actualPage}</Value>
      <SmallButton
        icon="chevron-right"
        onPress={nextPage}
        disabled={actualPage === totalPages}
      />
    </PaginationWrapper>
  );
};

export default Pagination;
