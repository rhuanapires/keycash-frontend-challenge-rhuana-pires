import {styled} from '~/modules';
import {getFontSize} from '~/utils';

export const PaginationWrapper = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  flex: 1;
`;

export const Value = styled.Text`
  ${getFontSize('callout')}
  color: ${props => props.theme.colors.text.lowDark};
  font-weight: bold;
`;
