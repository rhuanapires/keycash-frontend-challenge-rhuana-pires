import {FastImage, styled} from '~/modules';
import {getFontSize, getShadow} from '~/utils/theme';

export const CardWrapper = styled.TouchableOpacity`
  border-radius: ${props => props.theme.radius.mediumRadius}px;
  background-color: ${props => props.theme.colors.text.highPure};
  ${getShadow()};
`;

export const ContentWrapper = styled.View`
  padding: 10px 10px;
`;

export const Address = styled.Text`
  ${getFontSize('title4')};
  color: ${props => props.theme.colors.text.lowDark};
  font-weight: bold;
`;

export const Value = styled.Text`
  ${getFontSize('callout')};
  font-weight: bold;
  color: ${props => props.theme.colors.text.lowMedium};
`;

export const Thumbnail = styled(FastImage).attrs({
  resizeMode: 'cover',
})`
  width: 100%;
  min-height: 200px;
  border-radius: ${props => props.theme.radius.mediumRadius}px;
`;

export const CategoriesWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 5px 10px;
`;
