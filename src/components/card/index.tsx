import React, {FC} from 'react';
import {Routes} from '~/utils/enums';
import {navigate} from '../../navigation/actions';
import TextCategory from '../text-category';
import {
  Address,
  CardWrapper,
  CategoriesWrapper,
  ContentWrapper,
  Thumbnail,
  Value,
} from './styles';

type Props = {
  property: Property;
};

const Card: FC<Props> = ({property}) => {
  return (
    <CardWrapper onPress={() => navigate(Routes.PROPERTY_DETAILS, {property})}>
      <Thumbnail source={{uri: property.images[0]}} />
      <ContentWrapper>
        <Address>{property.address.formattedAddress}</Address>
        <Value>R$ {property.price},00</Value>
        <CategoriesWrapper>
          <TextCategory title={`${property.usableArea}m²`} icon="ruler" />
          <TextCategory title={property.bedrooms} icon="bed" />
          <TextCategory title={property.bathrooms} icon="shower" />
        </CategoriesWrapper>
      </ContentWrapper>
    </CardWrapper>
  );
};

export default Card;
