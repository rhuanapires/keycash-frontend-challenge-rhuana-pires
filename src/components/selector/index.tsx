import React, {FC} from 'react';
import SmallButton from '../small-button';
import {OptionsWrapper, SelectorWrapper, Title} from './styles';

type Props = {
  label: string;
  selected: number;
  onSelect: (key: number) => void;
};

const labelValues = [1, 2, 3];

const Selector: FC<Props> = ({label, selected, onSelect}) => {
  const GenerateRadioOptions = () =>
    labelValues.map((item, key) => (
      <SmallButton
        icon="plus"
        label={item}
        key={key}
        selected={key === selected}
        onPress={() => onSelect(key)}
      />
    ));

  return (
    <SelectorWrapper>
      <Title>{label}</Title>
      <OptionsWrapper>{GenerateRadioOptions()}</OptionsWrapper>
    </SelectorWrapper>
  );
};

export default Selector;
