import {styled} from '~/modules';
import {getFontSize} from '~/utils';

export const OptionsWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 10px;
`;

export const SelectorWrapper = styled.View`
  padding: 10px 0px;
`;

export const Title = styled.Text`
  ${getFontSize('callout')};
  color: ${props => props.theme.colors.text.lowMedium};
  font-weight: bold;
`;
