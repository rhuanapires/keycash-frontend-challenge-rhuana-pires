import React, {FC} from 'react';
import If from '../if';
import {BoldText, Content, Icon, Text} from './styles';

type Props = {
  title: string | number;
  icon: string;
  name?: string;
};

const TextCategory: FC<Props> = ({title, icon, name}) => {
  return (
    <>
      <If condition={!!name}>
        <BoldText>{name} :</BoldText>
      </If>
      <Content hasName={!!name}>
        <Icon name={icon} />
        <Text>{title}</Text>
      </Content>
    </>
  );
};

export default TextCategory;
