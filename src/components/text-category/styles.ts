import F5Icon from 'react-native-vector-icons/FontAwesome5';
import {moderateScale, styled} from '~/modules';
import {getFontSize} from '~/utils';

type ContentProps = {
  hasName?: boolean;
};

export const Text = styled.Text`
  ${getFontSize('body')}
  color: ${props => props.theme.colors.text.lowMedium};
`;

export const BoldText = styled(Text)`
  font-weight: 700;
`;

export const Icon = styled(F5Icon)`
  color: ${props => props.theme.colors.ternary};
  font-size: ${moderateScale(20)}px;
  padding-right: 10px;
`;

export const Content = styled.View<ContentProps>`
  flex-direction: row;
  justify-content: ${props => (props.hasName ? 'flex-start' : 'space-around')};
  padding: 10px;
  margin: 10px 0px;
`;
