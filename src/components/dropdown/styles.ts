import SelectDropdown from 'react-native-select-dropdown';
import {styled} from '~/modules';

export const DropdownPicker = styled(SelectDropdown).attrs(props => ({
  dropdownStyle: {
    borderRadius: props.theme.radius.mediumRadius,
    padding: 0,
  },
  rowTextStyle: {
    fontSize: 14,
  },
  buttonStyle: {
    height: 35,
    borderRadius: props.theme.radius.mediumRadius,
    borderColor: props.theme.colors.primary,
    borderWidth: 1,
    backgroundColor: props.theme.colors.text.highDark,
  },
  buttonTextStyle: {
    fontSize: 14,
  },
  dropdownIconPosition: 'right',
}))``;
