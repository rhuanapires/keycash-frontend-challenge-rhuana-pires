import React, {FC} from 'react';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {DropdownPicker} from './styles';

type Props = {
  changeItemsByPage: (quantity: number) => void;
};

const Dropdown: FC<Props> = ({changeItemsByPage}) => {
  const pages = [5, 10, 15, 20];

  return (
    <DropdownPicker
      data={pages}
      onSelect={selectedItem => {
        changeItemsByPage(selectedItem);
      }}
      buttonTextAfterSelection={selectedItem => {
        return selectedItem;
      }}
      rowTextForSelection={item => {
        return item;
      }}
      defaultButtonText={'Itens por página'}
      renderDropdownIcon={() => (
        <FontAwesome5Icon name="chevron-down" color="#fE1187" size={16} />
      )}
    />
  );
};

export default Dropdown;
