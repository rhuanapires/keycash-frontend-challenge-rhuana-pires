import React from 'react';
import {LoadingContainer, LoadingMessage, SpinnnerLoading} from './styles';

// import { Container } from './styles';

const LoadingPage: React.FC = () => {
  return (
    <LoadingContainer>
      <SpinnnerLoading />
      <LoadingMessage>Carregando</LoadingMessage>
    </LoadingContainer>
  );
};

export default LoadingPage;
