import Spinner from 'react-native-spinkit';
import {styled} from '~/modules';
import {getFontSize} from '~/utils';

export const LoadingMessage = styled.Text`
  ${getFontSize('callout')}
  color: ${props => props.theme.colors.text.lowMedium};
  padding-top: 20px;
`;

export const LoadingContainer = styled.View`
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const SpinnnerLoading = styled(Spinner).attrs(props => ({
  size: 40,
  type: 'ThreeBounce',
  color: props.theme.colors.terciary,
}))``;
