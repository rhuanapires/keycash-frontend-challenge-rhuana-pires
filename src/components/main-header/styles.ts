import {styled} from '~/modules';

export const HeaderWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 0px;
`;
