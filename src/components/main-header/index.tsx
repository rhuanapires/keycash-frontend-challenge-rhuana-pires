import React, {FC} from 'react';
import {navigate} from '~/navigation/actions';
import {Routes} from '~/utils/enums';
import If from '../if';
import InputSearch from '../input-search';
import Logo from '../logo';
import SmallButton from '../small-button';
import {HeaderWrapper} from './styles';

type Props = {
  onChangeInput: (value: string) => void;
  doSearch?: () => void;
  enableButton?: boolean;
  properties?: Property[];
};

const mainHeader: FC<Props> = ({
  doSearch,
  onChangeInput,
  enableButton = false,
  properties,
}) => {
  return (
    <>
      <If condition={enableButton}>
        <Logo />
      </If>
      <HeaderWrapper>
        <InputSearch
          placeholder="Buscar por endereço"
          onChangeText={onChangeInput}
          onPress={doSearch}
        />
        <If condition={enableButton}>
          <SmallButton
            icon="filter"
            onPress={() => navigate(Routes.SEARCH_TOOL, {properties})}
          />
        </If>
      </HeaderWrapper>
    </>
  );
};

export default mainHeader;
