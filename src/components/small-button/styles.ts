import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import styled from 'styled-components/native';
import {moderateScale} from '~/modules';
import {getShadow} from '~/utils';

type SelectedButton = {
  selected?: boolean;
};

export const ContainerButton = styled.TouchableOpacity<SelectedButton>`
  background-color: ${props =>
    props.selected
      ? props.theme.colors.primary
      : props.theme.colors.text.highDark};
  justify-content: center;
  padding: 8px 10px;
  border-radius: ${props => props.theme.radius.mediumRadius}px;
  border: 1px solid ${props => props.theme.colors.primary};
  align-items: center;
  opacity: ${props => (props.disabled ? 0.3 : 1)};
  ${getShadow()};
`;

export const Content = styled.View`
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;

export const Icon = styled(MaterialIcon)<SelectedButton>`
  color: ${props =>
    props.selected
      ? props.theme.colors.text.highPure
      : props.theme.colors.primary};
  font-size: ${moderateScale(17)}px;
`;

export const TextButton = styled.Text<SelectedButton>`
  color: ${props =>
    props.selected
      ? props.theme.colors.text.highPure
      : props.theme.colors.primary};
  font-weight: 700;
`;
