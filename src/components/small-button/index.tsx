import React, {FC} from 'react';
import {TouchableOpacityProps} from 'react-native';
import If from '../if';
import Loading from '../loading';
import {ContainerButton, Content, Icon, TextButton} from './styles';

type Props = TouchableOpacityProps & {
  icon: string;
  label?: number;
  loading?: boolean;
  disabled?: boolean;
  selected?: boolean;
};

const SmallButton: FC<Props> = ({
  icon,
  loading,
  disabled,
  label,
  selected,
  ...props
}) => {
  return (
    <ContainerButton
      disabled={disabled || loading}
      selected={selected}
      {...props}>
      <Content>
        <If condition={loading || false}>
          <Loading />
        </If>
        <If condition={!loading}>
          <TextButton selected={selected}>{label}</TextButton>
          <Icon selected={selected} name={icon} />
        </If>
      </Content>
    </ContainerButton>
  );
};

export default SmallButton;
