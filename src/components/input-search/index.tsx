import React, {FC} from 'react';
import {TextInputProps, TouchableOpacityProps} from 'react-native';
import If from '../if';
import SearchButton from '../search-button';

import {ContainerTextInput, TextInput} from './styles';

type Props = TextInputProps & TouchableOpacityProps;

const InputSearch: FC<Props> = ({onPress, ...props}) => {
  return (
    <ContainerTextInput>
      <TextInput {...props} />
      <If condition={!!onPress}>
        <SearchButton icon="magnify" onPress={onPress} />
      </If>
    </ContainerTextInput>
  );
};

export default InputSearch;
