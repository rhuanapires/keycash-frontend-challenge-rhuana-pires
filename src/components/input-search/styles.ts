import {moderateScale, styled} from '~/modules';
import {getFontSize, getShadow} from '~/utils';

export const TextInput = styled.TextInput`
  height: ${moderateScale(35)}px;
  ${getFontSize('callout')};
  margin: 0px 15px;
  flex: 1;
`;

export const ContainerTextInput = styled.View`
  flex-direction: row;
  background-color: ${props => props.theme.colors.text.highPure};
  border-radius: ${props => props.theme.radius.largeRadius}px;
  justify-content: center;
  align-items: center;
  margin-right: 10px;
  padding: 5px;
  flex: 1;
  ${getShadow()};
`;
