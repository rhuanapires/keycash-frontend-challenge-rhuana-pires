import React, {FC} from 'react';
import {createNativeStackNavigator} from '~/modules';
import {Home, PropertyDetails, SearchTool} from '~/pages';
import {Routes} from '~/utils/enums';

const Stack = createNativeStackNavigator();

const Navigation: FC = () => {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerStyle: {backgroundColor: '#fE1187'},
        headerTintColor: '#fff',
      }}>
      <Stack.Screen
        name={Routes.HOME}
        component={Home}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={Routes.PROPERTY_DETAILS}
        component={PropertyDetails}
        options={{title: 'Detalhes do Imóvel'}}
      />
      <Stack.Screen
        name={Routes.SEARCH_TOOL}
        component={SearchTool}
        options={{title: 'Buscar Imóveis'}}
      />
    </Stack.Navigator>
  );
};

export default Navigation;
