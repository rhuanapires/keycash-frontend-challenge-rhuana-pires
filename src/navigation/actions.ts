import {NavigationContainerRef} from '@react-navigation/core';
import {NavigationParams} from '~/utils/types';

type RouteParams = Record<string, any>;

let navigator: NavigationContainerRef<NavigationParams>;

export function setTopLevelNavigator(
  navigatorRef: NavigationContainerRef<NavigationParams>,
): void {
  navigator = navigatorRef;
}

export function navigate(name: string, params?: RouteParams) {
  navigator.navigate(name, params);
}

export function goBack() {
  navigator.goBack();
}
