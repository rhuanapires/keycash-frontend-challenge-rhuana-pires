import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer, ThemeProvider} from '~/modules';
import theme from '~/utils/theme';
import Navigation from './src/navigation';
import {setTopLevelNavigator} from './src/navigation/actions';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <StatusBar barStyle="dark-content" backgroundColor="#fE1187" />
      <NavigationContainer ref={setTopLevelNavigator}>
        <Navigation />
      </NavigationContainer>
    </ThemeProvider>
  );
};

export default App;
